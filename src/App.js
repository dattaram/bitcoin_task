import React from 'react';
import logo from './logo.svg';
import './App.css';
import ChartMain from './component/ChartMain';
import ChartMainTwo from './component/ChartMainTwo';

function App() {
  return (
    <div className="App">
      {/* <ChartMain/> */}
      <ChartMainTwo />
    </div>
  );
}

export default App;
