import React, { Component } from 'react'
import { createChart } from 'lightweight-charts';

export default class ChartMainTwo extends Component {
    constructor(props){
        super(props)
        this.state={
            main:[
            ]
        }
    }
    componentDidMount(){
        const chartProperties = {
            width:1000,
            height:600,
            timeScale:{
                timeVisible:true,
                secondsVisible:false,
            }
            }
            
            const domElement = document.getElementById('tvchart');
            const chart = createChart(domElement,chartProperties);
            const areaSeries = chart.addAreaSeries();
            setInterval(()=>{
            fetch(`https://api.cryptonator.com/api/ticker/btc-usd`)
            .then(res => res.json())
            .then(data => {
                const date= new Date()
                const dateFormat=date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate() +' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds()
                const mainData={
                    time:data.timestamp,
                    value:data.ticker.price
                }
                this.setState({
                    main:[...this.state.main,mainData]
                })
                areaSeries.setData(this.state.main.map(data => {
                    const date= new Date()
                    const dateFormat=date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate() +' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds()
                    return{
                        time:data.time,
                        value:parseFloat(data.value)
                    }
                }));
            })
            .catch(err => console.log(err))
        },60000)
        }
    render() {
        return (
            <div id='tvchart'>
                
            </div>
        )
    }
}
