import React, { Component } from 'react'
import Plot from 'react-plotly.js';
import { Chart } from "react-google-charts";

export default class ChartMain extends Component {
    constructor(props) {
        super(props);
        this.state = {
          data: [],
          ex:[],
          xValue:[],
          yValue:[],
          base:'',
          target:'',
          price:'',
          change:''
        };
      }

      componentDidMount() {
        setInterval(()=>{
          fetch(`https://api.cryptonator.com/api/ticker/btc-usd`)
            .then(response => response.json())
            .then(data => {
              this.setState({
               data: [...this.state.data,{
                  date: new Date(),
                  price: data.ticker.price
                }],
                xValue:this.state.data.map(data=>(
                  data.date
                )),
                yValue:this.state.data.map(data=>(
                  data.price
                )),
                base:data.ticker.base,
                target:data.ticker.target,
                price:data.ticker.price,
                change:data.ticker.change
              });
          })
           .catch(error => console.log(error));
        },5000)
      }

    render() {
        console.log(this.state)
        const data=this.state.data.map(data=>([data.date.toString(),parseFloat(data.price)]))
        const mainData=[['Year','Sales'],...data]
        return (
            <div>
                <h2>{this.state.base} / {this.state.target} </h2>
                <h4>{this.state.price}</h4>
                {/* <p>{this.state.change} </p> */}
                <Plot
                    data={[
                        {
                        x: this.state.xValue,
                        y: this.state.yValue,
                        type: 'scatter',
                        mode: 'lines+markers',
                        marker: {color: 'red'},
                        }
                    ]}
                    layout={{width: 1020, height: 440, title: 'Bitcoin Chart'}}
                />

                <Chart
                    width={'1024px'}
                    height={'800px'}
                    chartType="AreaChart"
                    loader={<div>Loading Chart</div>}
                    data={mainData}
                    options={{
                    title: 'Bitcoin Chart',
                    hAxis: { title: 'Current Date', titleTextStyle: { color: '#333' } },
                    vAxis: {  },
                    // For the legend to fit, we make the chart area smaller
                    chartArea: { width: '50%', height: '70%' },
                    // lineWidth: 25
                    }}
                    // For tests
                    rootProps={{ 'data-testid': '1' }}
                />
            </div>
        )
    }
}
